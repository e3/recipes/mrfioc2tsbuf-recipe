#!/bin/bash

LIBVERSION=${PKG_VERSION}

# This is the directory that the ESS-specific scripts are extracted into
export ESS_MRFIOC2_DIR=ess

# Clean between variants builds
make -f Makefile.E3 clean

make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION}
# copy over ess specific substitutions
# not sure why this isn't done in the Makefile but in configure/module/RULES_MODULE...
install -m 644 ${ESS_MRFIOC2_DIR}/template/evg-*-ess.substitutions  evgMrmApp/Db/
install -m 644 ${ESS_MRFIOC2_DIR}/template/evm-*-ess.substitutions  evgMrmApp/Db/
install -m 644 ${ESS_MRFIOC2_DIR}/template/evr-*-ess.substitutions  evrMrmApp/Db/
install -m 644 ${ESS_MRFIOC2_DIR}/template/evr-databuffer-*.template  evrMrmApp/Db/
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} db
make -f Makefile.E3 MODULE=${PKG_NAME} LIBVERSION=${LIBVERSION} install

# install the ess iocsh scripts into the directory
install -m 644 ess-iocsh/*.iocsh  ${PREFIX}/modules/${PKG_NAME}/${PKG_VERSION}/